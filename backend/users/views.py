from datetime import datetime, timedelta, timezone

from django.http import JsonResponse
from django.shortcuts import render
from game.exceptions import BaseGameException, SUCCESS, FAILURE

from .decorators import (
    decorate_set_game_session,
    decorate_set_user_token,
    decorate_game,
)
from .models import GameSession, User
from gameboard.models import UserMove


@decorate_set_game_session
def set_game_session(request, uid):
    session = GameSession.objects.filter(uid=uid)
    if session:
        session = session[0]
    else:
        raise BaseGameException("Game session does not exist", FAILURE)
    if session.cancelled:
        raise BaseGameException("Game session is cancelled", FAILURE)
    time_left = session.time_left
    if session.started_at and not time_left:
        raise BaseGameException("Game session is over", FAILURE)
    response = BaseGameException("Cookie is set", SUCCESS).json_response()
    response.set_cookie("gsession", uid)
    return response


@decorate_set_user_token
def set_user_token(request, username, utoken, uid):
    users = User.objects.filter(uname=username)

    if uid:
        try:
            session = GameSession.objects.get(uid=uid)
        except GameSession.DoesNotExist:  # Invalid game session
            return JsonResponse({"redirect": "start"})
    else:  # utoken
        try:
            user = users.get(token=utoken)
            raise BaseGameException("Cookie already set", SUCCESS)
        except User.DoesNotExist:
            pass

    try:  # Try to find an existing user
        user = users.get(game=session)
    except User.DoesNotExist:  # Create new user if doesn't exist
        game_users = User.objects.filter(game=session)
        if len(game_users) >= session.user_limit:
            raise BaseGameException("User limit reached", FAILURE)
        user = User.objects.create(game=session, uname=username)
        user.save()

    response = BaseGameException("Cookie is set", SUCCESS).json_response()
    response.set_cookie("utoken", user.token, max_age=session.time_left)
    return response


@decorate_game
def unstart(request, user):
    user.game.started_at = None
    user.game.save()
    user.game.user_set.update(ready=False)
    UserMove.objects.filter(user__in=user.game.user_set.all()).delete()
    return JsonResponse("ok", safe=False)


@decorate_game
def get_ready_users(request, user):
    game = user.game
    all_users = game.user_set.values("ready", "uname")

    return JsonResponse(
        {
            "started_at": game.started_at,
            "users": {x["uname"]: x["ready"] for x in all_users},
        }
    )


@decorate_game
def user_ready(request, user):
    game = user.game
    user.ready = True
    user.save()
    if not game.started_at:
        all_ready = all(game.user_set.values_list("ready", flat=True))
        if all_ready:
            game.started_at = datetime.now(timezone.utc) + timedelta(
                seconds=game.pre_game_time_limit
            )
            game.save()
    return JsonResponse("ok", safe=False)


@decorate_game
def user_details(request, user):
    return JsonResponse(
        {
            "session_id": user.game_id,
            "username": user.uname,
            "points": user.points,
            "ready": user.ready,
        }
    )


@decorate_game
def session_details(request, user):
    game = user.game
    return JsonResponse(
        {
            "session_id": game.uid,
            "time_limit": game.time_limit,
            "user_limit": game.user_limit,
            "started_at": game.started_at,
            "cancelled": game.cancelled,
        }
    )


@decorate_game
def details(request, user):
    game = user.game
    return JsonResponse(
        {
            "session_id": game.uid,
            "username": user.uname,
            "ready": user.ready,
            "time_limit": game.seconds,
            "user_limit": game.user_limit,
            "started_at": game.started_at,
            "cancelled": game.cancelled,
            "rows": game.rows,
            "cols": game.cols,
            "max_words": game.max_words,
            "max_chars": game.max_chars,
            "max_img": game.max_img,
            "total_moves": game.total_moves,
        }
    )
