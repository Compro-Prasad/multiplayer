import uuid
from datetime import datetime, time, timezone

from django.db import models
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _


class GameSession(models.Model):
    uid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    time_limit = models.TimeField(verbose_name="Time Limit(s)", default=time(minute=5))
    user_limit = models.PositiveIntegerField(
        default=2, validators=[MinValueValidator(2)]
    )
    cancelled = models.BooleanField(default=False)
    started_at = models.DateTimeField(null=True, blank=True)

    rows = models.PositiveIntegerField(default=50)
    cols = models.PositiveIntegerField(default=50)
    max_words = models.PositiveIntegerField(default=100)
    max_chars = models.PositiveIntegerField(default=1500)
    max_img = models.PositiveIntegerField(default=15)
    total_moves = models.PositiveIntegerField(default=50)
    time_limit_error = models.TimeField(default=time(microsecond=250000))
    pre_game_time_limit = models.PositiveIntegerField(default=60)

    @property
    def time_left(self):
        if self.cancelled or not self.started_at:
            return
        time_elapsed = (datetime.now(timezone.utc) - self.started_at).total_seconds()
        time_left = self.seconds - int(time_elapsed)
        if time_left > 0:
            return time_left

    @property
    def seconds(self):
        t = self.time_limit
        return (t.hour * 3600) + (t.minute * 60) + t.second + (t.microsecond / 1000000)

    @property
    def seconds_error(self):
        t = self.time_limit_error
        return (t.hour * 3600) + (t.minute * 60) + t.second + (t.microsecond / 1000000)


class User(models.Model):
    game = models.ForeignKey(GameSession, on_delete=models.CASCADE)
    uname = models.CharField(verbose_name=_("Username"), max_length=16)
    token = models.UUIDField(
        editable=False, null=True, blank=True, unique=True, default=uuid.uuid4
    )
    ready = models.BooleanField(default=False)

    def _game_id(self):
        return self.game.uid

    def __str__(self) -> str:
        return self.uname
