from django.http import JsonResponse
from django.urls import resolve
from game.exceptions import BaseGameException, SUCCESS

from .models import User, GameSession


def decorate_set_game_session(func):
    assert func.__name__ == "set_game_session"

    def new_func(request, uid, *args, **kwargs):
        if request.COOKIES.get("gsession") == uid:
            return BaseGameException("Cookie already set", SUCCESS).json_response()
        try:
            return func(request, uid, *args, **kwargs)
        except BaseGameException as error:
            response = error.json_response()
            response.delete_cookie("gsession")
            return response

    return new_func


def decorate_set_user_token(func):
    assert func.__name__ == "set_user_token"

    def new_func(request, *args, **kwargs):
        try:
            utoken = request.COOKIES.get("utoken")
            uid = request.COOKIES.get("gsession")
            if not uid and not utoken:
                return JsonResponse({"redirect": "start"})
            response = func(request, *args, uid=uid, utoken=utoken, **kwargs)
        except BaseGameException as error:
            response = error.json_response()
        response.delete_cookie("gsession")
        return response

    return new_func


def decorate_game(func):
    def new_func(request, *args, **kwargs):
        token = request.COOKIES.get("utoken")
        if not token:
            return JsonResponse(
                {"redirect": "user" if request.COOKIES.get("gsession") else "start"}
            )
        try:
            user = User.objects.get(token=token)
        except User.DoesNotExist:
            return JsonResponse({"redirect": "start"})
        try:
            return func(request, *args, user=user, **kwargs)
        except BaseGameException as error:
            return error.json_response()

    return new_func
