from django.urls import path, include, re_path

from .views import set_game_session, set_user_token
from .views import session_details, user_details, details
from .views import get_ready_users, user_ready, unstart


session_urls = [
    re_path(r'set/(?P<uid>[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12})/$', set_game_session, name='set_session'),
    path('details/', session_details, name='session_details'),
]


user_urls = [
    re_path('set/(?P<username>[a-zA-Z][a-zA-Z_0-9-]{0,15})/$', set_user_token, name='set_token'),
    path('details/', user_details, name='user_details'),
    path('ready/', user_ready, name='user_ready')
]


urlpatterns = [
    path('session/', include(session_urls)),
    path('user/', include(user_urls)),
    path('unstart/', unstart, name='unstart'),
    path('users/ready/', get_ready_users, name='ready_users'),
    path('details/', details, name='details'),
]
