#!/bin/sh

PYTHON=${PYTHON:-python}

$PYTHON manage.py makemigrations

if [ ! -e db.sqlite3 ]; then
    $PYTHON manage.py migrate
    $PYTHON manage.py createsuperuser
else
    $PYTHON manage.py migrate
fi

$PYTHON manage.py runserver $1
