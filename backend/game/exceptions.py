from django.http import JsonResponse, HttpResponse


SUCCESS = 'ok'
FAILURE = 'failed'


class BaseGameException(BaseException):
    def __init__(self, message, status, code=None):
        assert status in [SUCCESS, FAILURE]
        code = code or (200 if status == SUCCESS else 422)
        super().__init__(message, status, code)
        self.message = message
        self.status = status
        self.code = code
    def ok(self):
        return self.code == 200
    def json_response(self):
        return JsonResponse(
            {
                'status': self.status,
                'message': self.message
            },
            status=self.code
        )
    def http_response(self):
        return HttpResponse(repr(self), status=self.code)
    def __str__(self):
        return f'{self.code}: {self.message}'
    def __repr__(self):
        return f'Game<{self}>'
