from gameboard.models import MoveDetail
from django.contrib import admin

from .models import UserMove, MoveDetail


@admin.register(MoveDetail)
class MoveDetailAdmin(admin.ModelAdmin):
    readonly_fields = ["read_end_time"]

    def read_end_time(self, obj):
        return str(obj.end_time)

    read_end_time.short_description = "End time"


admin.site.register(UserMove)
