import json
from collections import OrderedDict
from datetime import datetime, timezone

from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

from game.exceptions import BaseGameException, SUCCESS, FAILURE
from users.decorators import decorate_game

from .models import UserMove, MoveDetail


def home(request):
    return render(request, "dist/index.html")


@decorate_game
def get_game_moves(request, user):
    moves = MoveDetail.objects.filter(session=user.game).values(
        "move_type", "value", "end_time", "time_limit", "serial_no"
    )
    return JsonResponse(
        OrderedDict(sorted({move["serial_no"]: move for move in moves}.items()))
    )


@csrf_exempt
@decorate_game
def get_next_serial_no(request, user):
    now = datetime.now(timezone.utc)
    data = json.loads(request.body)
    serial_no = data.get("serial_no")
    value = data.get("value")
    conditions = {"end_time__gt": datetime.now(timezone.utc)}
    if serial_no:
        conditions["serial_no__gt"] = serial_no
    try:
        next_serial_number = (
            user.game.movedetail_set.filter(**conditions)
            .order_by("serial_no")[0]
            .serial_no
        )
    except IndexError:
        next_serial_number = None
    if not serial_no:
        return JsonResponse({"serial_no": next_serial_number}, safe=False)
    if user.usermove_set.filter(serial_no=serial_no):
        raise BaseGameException("User has already made this move", FAILURE)
    try:
        move = user.game.movedetail_set.get(serial_no=serial_no)
    except MoveDetail.DoesNotExist:
        raise BaseGameException("Move with the given serial_no doesnot exist", FAILURE)
    status, points = move.get_status_and_points(value, now)
    user_move = UserMove.objects.create(
        user=user,
        serial_no=serial_no,
        value=value,
        end_time=now,
        status=status,
        points=points,
    )
    user_move.save()
    return JsonResponse(
        {
            "serial_no": next_serial_number,
            "points": points,
            "status": UserMove.STATUS_CHOICES[status],
        }
    )
