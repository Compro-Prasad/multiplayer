import re
from datetime import datetime, timedelta

from django.db import models
from django.dispatch import receiver

from users.models import GameSession, User


class MoveDetail(models.Model):
    """Move definitions for a game session."""

    session = models.ForeignKey(GameSession, on_delete=models.CASCADE)
    move_type = models.CharField(
        choices=(
            ("click", "Click"),
            ("write", "Write"),
            ("simage", "Select Image"),
            ("pimage", "Predict Image"),
        ),
        max_length=20,
    )
    value = models.TextField()
    min_points = models.DecimalField(max_digits=22, decimal_places=2, default=1)
    max_points = models.DecimalField(max_digits=22, decimal_places=2, default=5)
    incorrect_points = models.DecimalField(max_digits=22, decimal_places=2, default=-1)
    incomplete_points = models.DecimalField(max_digits=22, decimal_places=2, default=0)
    serial_no = models.PositiveIntegerField()
    time_limit = models.TimeField()
    end_time = models.DateTimeField(null=True, blank=True, editable=False)

    class Meta:
        ordering = ("session", "serial_no", "move_type")

    def words(self, string):
        return re.split(r"\s+", string)

    def coordinates(self, string):
        values = string.split(";")
        result = [(None, None)] * len(values)
        for i, val in enumerate(values):
            x = val.split(",")
            result[i] = int(x[0]), int(x[1])
        return result

    @property
    def seconds(self):
        """Return the number of seconds in the time_limit."""
        t = self.time_limit
        return (t.hour * 3600) + (t.minute * 60) + t.second + (t.microsecond / 1000000)

    @property
    def prev_move(self):
        """Get the previous move as per serial_no of the game session.

        Return None if current move is the first move.
        """
        try:
            MoveDetail.objects.filter(
                session=self.session, serial_no__lt=self.serial_no
            ).order_by("-serial_no")[0]
        except IndexError:
            return None

    @property
    def start_time(self):
        try:
            return self.prev_move.end_time
        except AttributeError:
            return self.session.started_at

    @property
    def points_diff(self):
        return float(self.max_points - self.min_points)

    @property
    def next_move(self):
        """Get the next move as per serial_no of the game session.

        Return None if current move is the last move.
        """
        try:
            MoveDetail.objects.filter(
                session=self.session, serial_no__gt=self.serial_no
            ).order_by("serial_no")[0]
        except IndexError:
            return None

    def get_status_and_points(self, value, now):
        end_time = self.end_time + timedelta(self.session.seconds_error)

        if now > end_time:
            return UserMove.STATUS_NOT_ON_TIME, self.incomplete_points

        start_time = self.start_time
        time_spent = now - start_time
        time_limit = end_time - start_time
        time_percent = 1 - time_spent / time_limit
        points = float(self.min_points)

        if self.move_type == "click":

            true_values = self.coordinates(self.value)
            user_values = self.coordinates(value)

            if len(true_values) != len(user_values):
                return UserMove.STATUS_INCORRECT, self.incorrect_points

            correct_percent = sum(
                uval == tval for uval, tval in zip(user_values, true_values)
            ) / len(true_values)

            if correct_percent == 1:
                points += time_percent * self.points_diff
                status = UserMove.STATUS_COMPLETE
            elif correct_percent == 0:
                points = self.incorrect_points
                status = UserMove.STATUS_INCORRECT
            else:
                points += time_percent * correct_percent * self.points_diff
                status = UserMove.STATUS_PARTIAL

        elif self.move_type == "write":

            if self.value == value:  # TODO: Write better checker for diff
                points += time_percent * self.points_diff
                status = UserMove.STATUS_COMPLETE
            else:
                return UserMove.STATUS_INCORRECT, self.incorrect_points

        else:
            raise NotImplementedError(f"{self.move_type}")
        return status, points


@receiver(models.signals.post_save, sender=MoveDetail)
def modify_moves_on_md_change(sender, instance, *args, **kwargs):
    """Update MoveDetail.end_time when another move has changed."""
    movedetails = instance.session.movedetail_set
    try:
        # Previous move's end_time is next move's start time
        start_time = (
            movedetails.filter(serial_no__lt=instance.serial_no)
            .order_by("-serial_no")[0]
            .end_time
        )
    except IndexError:
        start_time = instance.session.started_at
    if not start_time:
        return
    moves = movedetails.all()
    for move in moves:  # Update all moves' end_time
        start_time = start_time + timedelta(seconds=move.seconds)
        move.end_time = start_time
    MoveDetail.objects.bulk_update(moves, ["end_time"])


@receiver(models.signals.post_save, sender=GameSession)
def modify_moves_on_gs_change(sender, instance, *args, **kwargs):
    """Update MoveDetail.end_time when related game session has changed."""
    start_time = instance.started_at
    if not start_time:
        # Remove end_time if no start_time provided
        instance.movedetail_set.update(end_time=None)
        return
    moves = instance.movedetail_set.all()
    for move in moves:  # Update all moves' end_time
        start_time = start_time + timedelta(seconds=move.seconds)
        move.end_time = start_time
    MoveDetail.objects.bulk_update(moves, ["end_time"])


class UserMove(models.Model):
    """Store all moves made by an user."""

    STATUS_INCORRECT = -1
    STATUS_NOT_ON_TIME = 0
    STATUS_PARTIAL = 1
    STATUS_COMPLETE = 2

    STATUS_CHOICES = {
        STATUS_INCORRECT: "Incorrect",
        STATUS_NOT_ON_TIME: "Not on time",
        STATUS_PARTIAL: "Partial",
        STATUS_COMPLETE: "Complete",
    }

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    serial_no = models.PositiveIntegerField()
    value = models.TextField()
    end_time = models.DateTimeField(default=datetime.utcnow)
    status = models.SmallIntegerField(choices=STATUS_CHOICES.items())
    points = models.DecimalField(max_digits=22, decimal_places=2)

    def __repr__(self) -> str:
        return f"User<{self.user.uname}: {self.user.game.uid}>"

    def __str__(self) -> str:
        return self.user.uname
