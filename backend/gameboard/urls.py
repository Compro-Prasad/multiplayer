from django.urls import path

from .views import get_game_moves, get_next_serial_no


urlpatterns = [
    path('game/moves/', get_game_moves, name='get_game_moves'),
    path('user/move/serial_no/next/', get_next_serial_no, name='get_next_serial_no')
]
