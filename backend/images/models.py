from django.db import models
from django.db.models.lookups import EndsWith


class Image(models.Model):
    img = models.ImageField(upload_to='images')
    tags = models.TextField()

    def __repr__(self):
        return f'Image<{self.img}>'

    def __str__(self) -> str:
        return f'{self.img}'
