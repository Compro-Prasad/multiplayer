import { reactive } from 'vue'

let backendURL = 'http://localhost:8888/'

const moveComponent = {
  click: 'Grid',
  write: 'Type',
  simage: 'Image',
  pimage: 'PredictImage',
}

if (backendURL.endsWith('/'))
  backendURL = backendURL.slice(0, backendURL.length - 1)

export const store = {
  debug: true,

  state: reactive({
    message: 'Hello!',
    show: 'loading',
    details: {},
    currentTime: new Date(),
    gameOver: false,
    moves: {},
  }),

  get endTime() {
    const details = this.state.details
    if (!details.started_at) {
      return null
    }
    let time = new Date(details.started_at)
    time.setSeconds(details.time_limit)
    return time
  },

  getURL(path) {
    if (!path.startsWith('/'))
      path = '/' + path
    return backendURL + path
  },

  fetch(path, config = {}) {
    return fetch(path.startsWith('/') ? this.getURL(path) : path, {
      credentials: 'include',
      ...config
    })
  },

  getDetails() {
    this.fetch('/details/')
      .then(res => res.json())
      .then(res => {
        if ('redirect' in res) {
          this.state.show = res.redirect
        } else {
          this.state.details = res
          if (res.started_at) {
            this.state.details.started_at = new Date(res.started_at)
            this.setMoves()
            this.setNextMove()
          }
          this.state.show = 'game'
        }
      })
      .catch(res => console.log(res))
  },

  setSession(uid) {
    this.fetch('/session/set/' + uid + '/')
      .then(res => res.json())
      .then(res => {
        if ('redirect' in res) {
          this.state.show = res.redirect
        } else if (res.status === 'ok') {
          this.state.show = 'user'
          this.state.message = ''
        } else {
          this.state.message = res.message
        }
      })
      .catch(() => this.state.message = 'Malformed ID')
  },

  setUser(username) {
    this.fetch('/user/set/' + username + '/')
      .then(res => res.json())
      .then(res => {
        if ('redirect' in res) {
          this.state.show = res.redirect
        } else if (res.status === 'ok') {
          this.state.show = 'game'
          this.state.message = ''
          this.getDetails()
        } else {
          this.state.message = res.message
        }
      })
      .catch(() => this.state.message = 'Malformed username')
  },

  setMoves() {
    this.fetch('/game/moves/')
      .then(res => res.json())
      .then(res => {
        Object.values(res).forEach(x => {
          x.component = moveComponent[x.move_type]
          x.endTime = new Date(x.end_time)
        })
        this.state.moves = res
      })
  },

  setNextMove(serial_no = '', value = '') {
    this.fetch('/user/move/serial_no/next/', {
      body: JSON.stringify({
        serial_no,
        value,
      }),
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(res => res.json())
      .then(res => this.state.serial_no = res.serial_no)
      .catch(res => {
        console.log(res)
        this.state.serial_no = null
      })
  },
}
